package com.pierreduchemin.testUtils;

import com.pierreduchemin.utils.OsUtils;
import com.pierreduchemin.utils.TextFileUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ArchiversTestUtils {

    private ArchiversTestUtils() {
    }

    public static boolean createDummyGameFolder(Path tempPath) {
        String game0 = "game0";
        String game1 = "game1";

        try {
            Path path0 = Paths.get(tempPath + OsUtils.getFileSeparator() + game0 +
                    OsUtils.getFileSeparator() + "save_file");
            Files.createDirectories(path0.getParent());
            TextFileUtils.writeTextFile(path0, "dummy");

            Path path1 = Paths.get(tempPath + OsUtils.getFileSeparator() + game0 +
                    OsUtils.getFileSeparator() + "dummy_folder" + OsUtils.getFileSeparator() + "save_file");
            Files.createDirectories(path1.getParent());
            TextFileUtils.writeTextFile(path1, "dummy");


            Path path2 = Paths.get(tempPath + OsUtils.getFileSeparator() + game1 +
                    OsUtils.getFileSeparator() + "save_file");
            Files.createDirectories(path2.getParent());
            TextFileUtils.writeTextFile(path2, "dummy");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean areStructuresEquals(Path p1, Path p2) {
        return FileUtils.listFiles(p1.toFile(), FileFilterUtils.trueFileFilter(), null)
                .equals(FileUtils.listFiles(p2.toFile(), FileFilterUtils.trueFileFilter(), null));
    }
}
