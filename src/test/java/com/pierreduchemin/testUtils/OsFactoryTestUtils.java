package com.pierreduchemin.testUtils;

import com.pierreduchemin.data.configuration.model.GnuLinux;
import com.pierreduchemin.data.configuration.model.OS;
import com.pierreduchemin.data.configuration.model.Windows;

public class OsFactoryTestUtils {

    private OsFactoryTestUtils() {
    }

    public static OS getWindows() {
        return new Windows("Windows");
    }

    public static OS getGnuLinux() {
        return new GnuLinux("GNU/Linux");
    }
}
