package com.pierreduchemin.utils;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TextFileUtilsTest {

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void writeTextFile_null_path() {
        try {
            TextFileUtils.writeTextFile(null, "Test");
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void writeTextFile_null_content() {
        try {
            TextFileUtils.writeTextFile(Paths.get(OsUtils.getUserHome()), null);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void readWriteTextFile() {
        Path pathTestFile = Paths.get(OsUtils.getTempFolder() + OsUtils.getFileSeparator() + "abc.txt");
        try {
            TextFileUtils.writeTextFile(pathTestFile, "abc");
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

        try {
            String textFileContent = TextFileUtils.readTextFile(pathTestFile);

            assertEquals("abc", textFileContent);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

        if (!pathTestFile.toFile().delete()) {
            fail();
        }
    }
}