package com.pierreduchemin.utils;

import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.configuration.model.OS;
import com.pierreduchemin.testUtils.OsFactoryTestUtils;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class OsUtilsTest {

    @Test
    public void getOsName() {
        String osName = OsUtils.getOsName();

        assertNotNull(osName);
        assertFalse(osName.isEmpty());
    }

    @Test
    public void getOsVersion() {
        String osVersion = OsUtils.getOsVersion();

        assertNotNull(osVersion);
        assertFalse(osVersion.isEmpty());
    }

    @Test
    public void getOsArchitecture() {
        String osArchitecture = OsUtils.getOsArchitecture();

        assertNotNull(osArchitecture);
        assertFalse(osArchitecture.isEmpty());
    }

    @Test
    public void getFileSeparator() {
        String fileSeparator = OsUtils.getFileSeparator();

        assertNotNull(fileSeparator);
        assertFalse(fileSeparator.isEmpty());
    }

    @Test
    public void getUserHome() {
        String userHome = OsUtils.getUserHome();
        File file = new File(userHome);

        assertNotNull(userHome);
        assertFalse(userHome.isEmpty());
        assertTrue(file.isDirectory());
        assertTrue(file.canRead());
    }

    @Test
    public void getAccountName() {
        String accountName = OsUtils.getAccountName();

        assertNotNull(accountName);
        assertFalse(accountName.isEmpty());
    }

    @Test
    public void getTempFolder() {
        String tempFolder = OsUtils.getTempFolder();
        File file = new File(tempFolder);

        assertNotNull(tempFolder);
        assertFalse(tempFolder.isEmpty());
        assertTrue(file.isDirectory());
        assertTrue(file.canRead());
        assertTrue(file.canWrite());
    }

    @Test
    public void pathToTagPath() {
        String tagPath = "";
        if (Preferences.get().getOs().getOsFamily() == OS.OsFamily.UNIX) {
            tagPath = OsUtils.pathToTagPath(OsFactoryTestUtils.getGnuLinux(), OsUtils.getUserHome() + "/.config/game");
        } else if (Preferences.get().getOs().getOsFamily() == OS.OsFamily.WINDOWS) {
            tagPath = OsUtils.pathToTagPath(OsFactoryTestUtils.getWindows(), OsUtils.getUserHome() + "\\Documents\\My Games\\game");
        }

        assertEquals("[HOME].config[SEP]game", tagPath);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void pathToTagPath_null_os() {
        OsUtils.pathToTagPath(null, "/home/a_user/.config/game");
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void pathToTagPath_null_path() {
        OsUtils.pathToTagPath(Preferences.get().getOs(), null);
    }

    @Test
    public void pathToTagPath_invalid_path() {
        String an_invalid_path_abc = "an invalid path abc";
        String tagPath = OsUtils.pathToTagPath(Preferences.get().getOs(), an_invalid_path_abc);

        assertEquals(an_invalid_path_abc, tagPath);
    }

    @Test
    public void tagPathToPath() {
        String path = OsUtils.tagPathToPath(Preferences.get().getOs(), "[HOME].config[SEP]game");

        assertEquals(OsUtils.getUserHome() + "/.config/game", path);
    }
}