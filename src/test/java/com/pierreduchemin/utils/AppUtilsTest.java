package com.pierreduchemin.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppUtilsTest {

    @Test
    public void getAppVersion() {
        String appVersion = AppUtils.getAppVersion();

        assertNotNull(appVersion);
        assertFalse(appVersion.isEmpty());
        assertTrue(appVersion.matches("\\d+\\.\\d+(-(ALPHA|BETA))?"));
    }
}