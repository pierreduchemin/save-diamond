package com.pierreduchemin.utils;

import org.junit.Test;

public class BrowserUtilsTest {

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void openUrl_null_test() {
        BrowserUtils.openUrl(null);
    }
}