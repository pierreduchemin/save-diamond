package com.pierreduchemin.data.json.model;

import com.pierreduchemin.data.json.exceptions.InvalidGameSetException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class LibraryTest {

    @Test
    public void getInstance() {
        Library library = Library.getInstance();

        assertNotNull(library);
    }

    @Test
    public void setGames() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(100, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);

        assertEquals(3, library.getKnownGames().size());
    }

    @Test
    public void setGames_null() {
        Library library = Library.getInstance();
        library.setGames(null);

        assertTrue(library.getKnownGames().isEmpty());
    }

    @Test(expected = InvalidGameSetException.class)
    public void setGames_invalid() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(100, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);
    }

    @Test
    public void addGame() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);

        assertEquals(2, library.getKnownGames().size());

        library.addGame(new Game(100, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));

        assertEquals(3, library.getKnownGames().size());
    }

    @Test(expected = InvalidGameSetException.class)
    public void addGame_invalid() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);

        assertEquals(2, library.getKnownGames().size());

        library.addGame(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
    }

    @Test
    public void addGames() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(100, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);

        assertEquals(3, library.getKnownGames().size());

        List<Game> games2 = new ArrayList<>();
        games2.add(new Game(1000, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games2.add(new Game(10000, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.addGames(games2);

        assertEquals(5, library.getKnownGames().size());
    }

    @Test(expected = InvalidGameSetException.class)
    public void addGames_invalid() {
        Library library = Library.getInstance();
        List<Game> games = new ArrayList<>();
        games.add(new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        games.add(new Game(100, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.setGames(games);

        assertEquals(3, library.getKnownGames().size());

        List<Game> games2 = new ArrayList<>();
        games2.add(new Game(10, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>()));
        library.addGames(games2);
    }
}