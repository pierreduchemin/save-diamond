package com.pierreduchemin.data.json.model;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_invalid_id() {
        new Game(-1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_empty_name() {
        new Game(1, "", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_invalid_name() {
        new Game(1, "   ", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_invalid_name2() {
        new Game(1, "a-a", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());
    }

    @Test
    public void equals() {
        Game a_game = new Game(1, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());
        Game a_game1 = new Game(2, "A Game", new Game.Links("https://pcgamingwiki.com/wiki/a_game"), true, new HashMap<>());

        assertEquals(a_game, a_game1);
    }
}