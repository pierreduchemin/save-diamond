package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.json.exceptions.InvalidGameJsonException;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.utils.OsUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GameDeserializerTest {

    @Test
    public void deserialize_null() {
        GameDeserializer gameDeserializer = GameDeserializer.get();

        assertNotNull(gameDeserializer);
    }

    @Test(expected = JsonSyntaxException.class)
    public void deserialize_invalid_json() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Game.class, GameDeserializer.get());
        String json = "[{\"id\":0,\"name\":\"0 A.D.\",\"links\":{\"pcgamin";
        gsonBuilder.create().fromJson(json, Game[].class);
    }

    @Test(expected = InvalidGameJsonException.class)
    public void deserialize_invalid_game() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Game.class, GameDeserializer.get());
        // no id
        String json = "[{\"name\":\"0 A.D.\",\"links\":{\"pcgamingwiki\":" +
                "\"https://pcgamingwiki.com/wiki/0_A.D.\"},\"save_path\":[{\"os\":\"Linux\",\"paths\":" +
                "[\"[HOME].local/share/0ad\"]}]}]";
        gsonBuilder.create().fromJson(json, Game[].class);
    }

    @Test
    public void deserialize() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Game.class, GameDeserializer.get());
        String json = "[{\"id\":0,\"name\":\"0 A.D.\",\"links\":{\"pcgamingwiki\":\"https://pcgamingwiki.com/wiki/" +
                "0_A.D.\"},\"save_path\":[{\"os\":\"Linux\",\"paths\":[\"[HOME]/.local/share/0ad\"]}]},{\"id\":1," +
                "\"name\":\"10,000,000\",\"links\":{\"pcgamingwiki\":\"https://pcgamingwiki.com/wiki/10,000,000\"}," +
                "\"save_path\":[{\"os\":\"Linux\",\"paths\":[\"[HOME]/.config/unity3d/EightyEightGames/10000000\"]}]}]";
        Game[] games = gsonBuilder.create().fromJson(json, Game[].class);

        assertEquals(2, games.length);
        assertEquals(0, games[0].getId());
        assertEquals("0 A.D.", games[0].getName());
        assertEquals("0-0_A.D.", games[0].getTempFolderName());
        assertNotNull(games[0].getLinks());
        assertEquals("https://pcgamingwiki.com/wiki/0_A.D.", games[0].getLinks().getPcgamingwiki());
        assertEquals(OsUtils.tagPathToPath(Preferences.get().getOs(), "[HOME].local/share/0ad"),
                games[0].getSavePaths().get("Linux").get(0).toString());
    }
}