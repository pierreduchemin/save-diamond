package com.pierreduchemin.data.configuration;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.SevenZipSavesArchiveFormat;
import com.pierreduchemin.data.archivers.ZipSavesArchiveFormat;
import com.pierreduchemin.data.configuration.model.OS;
import org.junit.Test;

import static org.junit.Assert.*;

public class PreferencesTest {

    @Test
    public void get() {
        Preferences preferences = Preferences.get();

        assertNotNull(preferences);
    }

    @Test
    public void getOs() {
        OS os = Preferences.get().getOs();

        assertNotNull(os);
        assertEquals(OsFactory.getCurrentOs(), os);
    }

    @Test
    public void getSavesArchiveFormat() {
        SavesArchiveFormat savesArchiveFormat = Preferences.get().getDefaultSavesArchiveFormat();
        SevenZipSavesArchiveFormat sevenZipSavesArchiveFormat = new SevenZipSavesArchiveFormat();

        // Check default format
        assertEquals(sevenZipSavesArchiveFormat.getArchiveFormatName(), savesArchiveFormat.getArchiveFormatName());
    }

    @Test
    public void setSavesArchiveFormat() {
        String previousArchiveFormatName = Preferences.get().getDefaultSavesArchiveFormat().getArchiveFormatName();
        ZipSavesArchiveFormat savesArchiveFormat = new ZipSavesArchiveFormat();
        Preferences.get().setDefaultSavesArchiveFormat(savesArchiveFormat);

        String newArchiveFormatName = Preferences.get().getDefaultSavesArchiveFormat().getArchiveFormatName();
        assertNotEquals(previousArchiveFormatName, newArchiveFormatName);
        assertEquals(savesArchiveFormat.getArchiveFormatName(), newArchiveFormatName);
    }

    @Test
    public void isScanOnStart() {
        assertTrue(Preferences.get().isScanOnStart());
    }

    @Test
    public void setScanOnStart() {
        boolean oldStatus = Preferences.get().isScanOnStart();
        Preferences.get().setScanOnStart(!oldStatus);
        boolean newStatus = Preferences.get().isScanOnStart();
        Preferences.get().setScanOnStart(oldStatus);

        assertEquals(newStatus, !oldStatus);
    }

    @Test
    public void isAskBeforeErasing() {
        assertTrue(Preferences.get().isAskBeforeErasing());
    }
}