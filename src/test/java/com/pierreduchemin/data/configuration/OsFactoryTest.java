package com.pierreduchemin.data.configuration;

import com.pierreduchemin.data.configuration.model.OS;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class OsFactoryTest {

    @Test
    public void getCurrentOs() {
        OS currentOs = OsFactory.getCurrentOs();

        assertNotNull(currentOs);
    }
}