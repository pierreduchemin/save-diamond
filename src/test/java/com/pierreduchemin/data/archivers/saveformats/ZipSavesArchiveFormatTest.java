package com.pierreduchemin.data.archivers.saveformats;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.ZipSavesArchiveFormat;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.configuration.model.OS;
import com.pierreduchemin.testUtils.ArchiversTestUtils;
import com.pierreduchemin.testUtils.StreamGobbler;
import com.pierreduchemin.utils.TextFileUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class ZipSavesArchiveFormatTest {

    @Test
    public void getArchiveFormatName() {
        SavesArchiveFormat zipSavesArchiveFormat = new ZipSavesArchiveFormat();

        assertEquals(zipSavesArchiveFormat.getArchiveFormatName(), "ZIP");
    }

    @Test
    public void getArchiveExtension() {
        SavesArchiveFormat zipSavesArchiveFormat = new ZipSavesArchiveFormat();

        assertEquals(zipSavesArchiveFormat.getArchiveExtension(), ".zip");
    }

    @Test
    public void toArchive() throws IOException, InterruptedException {
        // Create dummy files
        OS os = Preferences.get().getOs();
        Path dummyGamesPath = Paths.get(os.getAppTempPath() + File.separator + "dummy_games");
        if (!ArchiversTestUtils.createDummyGameFolder(dummyGamesPath)) {
            fail("not able to create dummy files");
        }

        // Archive with toArchive
        Path javaArchivePath = Paths.get(os.getAppTempPath() + File.separator + "java_dummy_games.zip");
        ZipSavesArchiveFormat zipSavesArchiveFormat = new ZipSavesArchiveFormat();
        zipSavesArchiveFormat.toArchive(dummyGamesPath, javaArchivePath);

        // Extract archive with system
        String extractedDummyGames = os.getAppTempPath() + File.separator + "extracted_dummy_games";
        Process process = Runtime.getRuntime().exec(
                String.format("unzip -o %s -d %s",
                        javaArchivePath, extractedDummyGames));
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        assertEquals(0, exitCode);

        // Compare archive content with original folder
        Path extractedDummyGamesPath = Paths.get(extractedDummyGames);
        assertEquals(FileUtils.sizeOfDirectory(dummyGamesPath.toFile()), FileUtils.sizeOfDirectory(extractedDummyGamesPath.toFile()));
        assertTrue(ArchiversTestUtils.areStructuresEquals(dummyGamesPath, extractedDummyGamesPath));

        // Clean
        if (!javaArchivePath.toFile().delete()) {
            fail("not able to delete test archive at " + javaArchivePath);
        }
        FileUtils.deleteDirectory(extractedDummyGamesPath.toFile());
        FileUtils.deleteDirectory(dummyGamesPath.toFile());
    }

    @Test
    public void fromArchive() throws IOException {
        // Create dummy archive
        OS os = Preferences.get().getOs();
        Path dummyGamesPath = Paths.get(os.getAppTempPath() + File.separator + "dummy_games");
        if (!ArchiversTestUtils.createDummyGameFolder(dummyGamesPath)) {
            fail("not able to create dummy files");
        }
        Path javaArchivePath = Paths.get(os.getAppTempPath() + File.separator + "java_dummy_games.zip");
        ZipSavesArchiveFormat zipSavesArchiveFormat = new ZipSavesArchiveFormat();
        zipSavesArchiveFormat.toArchive(dummyGamesPath, javaArchivePath);

        // Extract with fromArchive
        Path extractedDummyGamesPath = Paths.get(os.getAppTempPath() + File.separator + "extracted_dummy_games");
        zipSavesArchiveFormat.fromArchive(javaArchivePath, extractedDummyGamesPath);

        // Check that all dummy files are present
        assertEquals(FileUtils.sizeOfDirectory(dummyGamesPath.toFile()), FileUtils.sizeOfDirectory(extractedDummyGamesPath.toFile()));
        assertTrue(ArchiversTestUtils.areStructuresEquals(dummyGamesPath, extractedDummyGamesPath));
        assertEquals("dummy", TextFileUtils.readTextFile(Paths.get(extractedDummyGamesPath + File.separator + "game0" + File.separator + "save_file")));

        // Clean
        if (!javaArchivePath.toFile().delete()) {
            fail("not able to delete test archive at " + javaArchivePath);
        }
        FileUtils.deleteDirectory(extractedDummyGamesPath.toFile());
        FileUtils.deleteDirectory(dummyGamesPath.toFile());
    }
}