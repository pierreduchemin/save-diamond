package com.pierreduchemin.domain;

import com.pierreduchemin.data.json.model.Game;

import java.io.File;

public interface UnitaryActions {

    void copyToTemp(Game game);

    void copyFromTemp(Game game);

    String getDescription(File tempGameDir);

    void readExtractedGameInfo();
}
