package com.pierreduchemin.domain;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.exceptions.InvalidArchiveException;
import com.pierreduchemin.data.archivers.exceptions.SaveAlreadyExistException;
import com.pierreduchemin.data.archivers.exceptions.TemporaryFilesNotFoundException;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.data.json.model.Library;
import com.pierreduchemin.utils.AppUtils;
import com.pierreduchemin.utils.LambdaExceptionUtils;
import com.pierreduchemin.utils.OsUtils;
import com.pierreduchemin.utils.TextFileUtils;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SaveOrganiserImpl {

    private static final String DESCRIPTION_FILE = "description.txt";
    private static final String PATH_FILE = "path.txt";
    private static final String PATH_PREFIX = "path_";

    private SaveOrganiserImpl() {
    }

    public static void fromArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull File archiveFile, boolean eraseExistingFiles)
            throws IOException, InvalidArchiveException {

        // Unarchive file in a temp folder
        Path tempPath = checkTempPath();
        savesArchiveFormat.fromArchive(archiveFile.toPath(), tempPath);

        // Check that archive is ok for host
        String descriptionFile = TextFileUtils.readTextFile(Paths.get(tempPath + File.separator + DESCRIPTION_FILE));
        String[] split = descriptionFile.split("_");
        if (split.length <= 1) {
            throw new InvalidArchiveException("Invalid description.txt file");
        }

        // Walk across game saves archive
        try {
            Files.list(tempPath)
                    .filter(Objects::nonNull)
                    .filter(path -> path.toFile().isDirectory())
                    .forEach(LambdaExceptionUtils.rethrowConsumer(path -> restoreFromTempFolder(path, eraseExistingFiles)));
        } finally {
            deleteTempFolder(tempPath);
        }
    }

    private static void restoreFromTempFolder(@NotNull Path gameTempFolder, boolean eraseExistingFiles)
            throws SaveAlreadyExistException, TemporaryFilesNotFoundException, IOException, IllegalStateException {

        File file = gameTempFolder.toFile();
        if (file == null) {
            throw new IllegalStateException("Unable to convert a Path to a File");
        }
        String[] list = file.list();
        if (list == null) {
            throw new IllegalStateException(file.getAbsolutePath() + " should be a folder");
        }

        // -1 is for description.txt file
        int fileNumber = list.length;
        for (int pathIndex = 1; pathIndex <= fileNumber; pathIndex++) {

            // Compose all path.txt paths based on name convention
            Path pathFile = Paths.get(gameTempFolder.toAbsolutePath() + File.separator + PATH_PREFIX + pathIndex + File.separator + PATH_FILE);

            Path destinationPath = Paths.get(OsUtils.tagPathToPath(Preferences.get().getOs(), TextFileUtils.readTextFile(pathFile)));

            Path dataToRestore = pathFile.resolveSibling(destinationPath.getFileName());

            File destinationFile = destinationPath.toFile();

            // If in force mode, removes existing files
            if (eraseExistingFiles) {
                if (destinationFile.exists()) {
                    FileUtils.forceDelete(destinationFile);
                }
            } else if (destinationFile.exists()) {
                throw new SaveAlreadyExistException("File already exist at " + destinationFile.getAbsolutePath(), destinationFile.getAbsolutePath());
            }

            try {
                FileUtils.forceMkdir(destinationFile.getParentFile());
                FileUtils.moveDirectory(dataToRestore.toFile(), destinationFile);
            } catch (IOException e) {
                if (eraseExistingFiles) {
                    // Stop restoration process
                    throw new TemporaryFilesNotFoundException("Not able to restore saves from temporary folder");
                }
            }
        }
    }

    public static void toArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull Path newArchiveFile, @NotNull Library library)
            throws IOException {

        Path tempPath = checkTempPath();

        // Copy game saves in app temp folder
        library.getKnownGames().stream()
                .filter(Game::isToSave)
                .forEach(LambdaExceptionUtils.rethrowConsumer(game -> {

                    // Create a temp folder for game
                    String tempFolderPath = tempPath.toFile().getAbsoluteFile() + File.separator + game.getTempFolderName();
                    File gameTempFolder = new File(tempFolderPath);
                    boolean mkdir = gameTempFolder.mkdir();
                    if (!mkdir) {
                        throw new IOException("Not able to create temp folder at " + gameTempFolder.getAbsoluteFile());
                    }

                    List<Path> savePaths = game.getSavePaths(Preferences.get().getOs());
                    for (int savePathIndex = 1; savePathIndex <= savePaths.size(); savePathIndex++) {
                        Path currentPath = savePaths.get(savePathIndex - 1);
                        if (currentPath.toFile().exists()) {
                            saveToTempFolder(savePathIndex, currentPath, gameTempFolder.toPath());
                        }
                    }
                }));

        // Create a txt file with archive date
        TextFileUtils.writeTextFile(
                Paths.get(tempPath + File.separator + DESCRIPTION_FILE),
                getArchiveDescription()
        );

        // Compress app temp folder to an archive
        try {
            savesArchiveFormat.toArchive(tempPath, newArchiveFile);
        } finally {
            deleteTempFolder(tempPath);
        }
    }

    @NotNull
    private static Path checkTempPath() throws IOException {
        Path tempPath = Preferences.get().getOs().getAppTempPath();
        File file = tempPath.toFile();
        if (!file.exists()) {
            boolean mkdir = file.mkdir();
            if (!mkdir) {
                throw new IOException("Invalid temp folder");
            }
        }
        if (!file.canWrite()) {
            throw new IOException("Non writable temp folder");
        }
        return tempPath;
    }

    /**
     * @param savePathIndex if game have multiple save folder, this is an id
     * @param srcPath       a path from game where saves are present
     * @param gameTempPath  game's temporary folder
     */
    private static void saveToTempFolder(int savePathIndex, @NotNull Path srcPath, @NotNull Path gameTempPath)
            throws IOException {

        // Create a temp folder for path
        String tempFolderPath = gameTempPath.toFile().getAbsoluteFile() + File.separator + PATH_PREFIX + savePathIndex;
        File savePathTempFolder = new File(tempFolderPath);
        boolean mkdir = savePathTempFolder.mkdir();
        if (!mkdir) {
            throw new IOException("Not able to create temp folder at " + savePathTempFolder.getAbsoluteFile());
        }

        Path directory = Files.createDirectory(Paths.get(savePathTempFolder + File.separator + srcPath.getFileName()));
        FileUtils.copyDirectory(srcPath.toFile(), directory.toFile());

        // Create a text file with instructions to restore save
        TextFileUtils.writeTextFile(
                Paths.get(savePathTempFolder.getAbsolutePath() + File.separator + PATH_FILE),
                OsUtils.pathToTagPath(Preferences.get().getOs(), srcPath.toString())
        );
    }

    @NotNull
    public static List<Game> scanSystem() {
        return Library.getInstance().getKnownGames().parallelStream()
                .filter(game -> game.getSavePaths(Preferences.get().getOs()).stream()
                        .anyMatch(path -> path.toFile().exists())
                )
                .collect(Collectors.toList());
    }

    private static void deleteTempFolder(@NotNull Path tempPath) throws IOException {
        FileUtils.deleteDirectory(tempPath.toFile());
    }

    @NotNull
    private static String getArchiveDescription() {
        return String.format("save-diamond_%s_%s_%d",
                AppUtils.getAppVersion(), Preferences.get().getOs().getReadableName(), System.currentTimeMillis()
        );
    }

    @NotNull
    public static List<Game> scanArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull File archiveFile)
            throws IOException, InvalidArchiveException, IllegalStateException {

        // Unarchive file in a temp folder
        Path tempPath = checkTempPath();
        savesArchiveFormat.fromArchive(archiveFile.toPath(), tempPath);

        // Check that archive is ok for host
        String descriptionFile = TextFileUtils.readTextFile(Paths.get(tempPath + File.separator + DESCRIPTION_FILE));
        String[] splitDescription = descriptionFile.split("_");
        if (splitDescription.length <= 1) {
            throw new InvalidArchiveException("Invalid description.txt file");
        }

        List<Game> result = new ArrayList<>();
        // Walk across game saves archive
        try {
            Files.list(tempPath)
                    .filter(Objects::nonNull)
                    .filter(path -> path.toFile().isDirectory())
                    .forEach(LambdaExceptionUtils.rethrowConsumer(path -> {

                        // read extracted game info
                        String[] splitFolderName = path.getFileName().toString().split("-");
                        int id = Integer.parseInt(splitFolderName[0]);
                        String name = splitFolderName[1];

                        HashMap<String, List<Path>> savePaths = new HashMap<>();
                        ArrayList<Path> pathList = new ArrayList<>();
                        savePaths.put(OsUtils.getOsName(), pathList);
                        Game currentGame = new Game(id, name, null, false, savePaths);
                        result.add(currentGame);

                        String[] currentFolderFiles = path.toFile().list();
                        if (currentFolderFiles == null) {
                            throw new IllegalStateException("Unable to list files in archive");
                        }
                        int fileNumber = currentFolderFiles.length;
                        for (int pathIndex = 1; pathIndex <= fileNumber; pathIndex++) {

                            // Compose all path.txt paths based on name convention
                            Path pathFile = Paths.get(path.toAbsolutePath() + File.separator + PATH_PREFIX + pathIndex + File.separator + PATH_FILE);
                            pathList.add(Paths.get(OsUtils.tagPathToPath(Preferences.get().getOs(), TextFileUtils.readTextFile(pathFile))));
                        }

                    }));
        } finally {
            deleteTempFolder(tempPath);
        }

        return result;
    }
}
