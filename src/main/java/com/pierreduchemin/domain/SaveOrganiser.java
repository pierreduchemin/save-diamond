package com.pierreduchemin.domain;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.exceptions.InvalidArchiveException;
import com.pierreduchemin.data.archivers.exceptions.SaveAlreadyExistException;
import com.pierreduchemin.data.archivers.exceptions.TemporaryFilesNotFoundException;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.data.json.model.Library;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface SaveOrganiser {


    void fromArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull File archiveFile, boolean eraseExistingFiles)
            throws IOException, InvalidArchiveException, TemporaryFilesNotFoundException, SaveAlreadyExistException;


    void toArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull Path newArchiveFile, @NotNull Library library)
            throws IOException;

    @NotNull
    List<Game> scanSystem();

    List<Game> getAllGames();
}
