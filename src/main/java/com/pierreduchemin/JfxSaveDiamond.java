package com.pierreduchemin;

import com.pierreduchemin.ui.actionmenu.ActionMenuViewImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ResourceBundle;

public class JfxSaveDiamond extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(@NotNull Stage primaryStage) throws IOException {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("default");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/action_menu.fxml"), resourceBundle);
        Parent root = loader.load();

        ActionMenuViewImpl actionMenuView = loader.getController();
        actionMenuView.setStage(primaryStage);

        primaryStage.setTitle(resourceBundle.getString("program_name"));
        primaryStage.getIcons().add(new Image("/icons/app_icon/app_icon_190.png"));
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(250);
        primaryStage.show();
    }
}
