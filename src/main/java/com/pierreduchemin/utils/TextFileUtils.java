package com.pierreduchemin.utils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class TextFileUtils {

    private TextFileUtils() {
    }

    public static void writeTextFile(@NotNull Path path, @NotNull String content) throws IOException {
        if (path == null || content == null) {
            throw new IllegalArgumentException();
        }
        Files.write(path, content.getBytes());
    }

    public static String readTextFile(@NotNull Path path) throws IOException {
        if (path == null) {
            throw new IllegalArgumentException();
        }
        return Files.lines(path).collect(Collectors.joining());
    }
}
