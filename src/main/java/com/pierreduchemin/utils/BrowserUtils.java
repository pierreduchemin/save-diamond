package com.pierreduchemin.utils;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BrowserUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrowserUtils.class);

    private static class CommandResult {
        int exitValue;
        String result;

        public CommandResult(int exitValue, String result) {
            this.exitValue = exitValue;
            this.result = result;
        }
    }

    private BrowserUtils() {
    }

    public static void openUrl(@NotNull String url) {
        if (url == null) {
            throw new IllegalArgumentException();
        }
        try {
            // Tries with default browser
            CommandResult commandResult = defaultBrowserLinux(url);
            if (commandResult.exitValue == 0) {
                LOGGER.info("Opening {} with default browser", url);
                return;
            }

            // Tries with known browsers one by one
            String[] knownBrowsers = {"firefox", "chromium-browser", "epiphany", "mozilla", "konqueror",
                    "netscape", "opera", "links", "lynx"};
            for (String browserCommand : knownBrowsers) {
                CommandResult browserPath = run(browserCommand, url);
                if (browserPath.exitValue == 0) {
                    LOGGER.info("Opening {} with {}", url, browserCommand);
                    return;
                }
            }
        } catch (IOException e) {
            //ignored
        }
    }

    private static CommandResult defaultBrowserLinux(String command) throws IOException {
        return run("xdg-open", command);
    }

    private static CommandResult run(String... commands) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(commands);

        try {
            proc.waitFor();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return new CommandResult(-1, "InterruptedException");
        }

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        return new CommandResult(proc.exitValue(), stdInput.readLine());
    }
}
