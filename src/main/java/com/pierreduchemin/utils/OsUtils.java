package com.pierreduchemin.utils;

import com.pierreduchemin.data.configuration.model.OS;
import com.pierreduchemin.data.configuration.model.SpecificPath;
import org.jetbrains.annotations.NotNull;

public class OsUtils {

    private OsUtils() {
    }

    public static String getOsName() {
        return System.getProperty("os.name");
    }

    public static String getOsVersion() {
        return System.getProperty("os.version");
    }

    public static String getOsArchitecture() {
        return System.getProperty("os.arch");
    }

    public static String getFileSeparator() {
        return System.getProperty("file.separator");
    }

    public static String getUserHome() {
        return System.getProperty("user.home");
    }

    public static String getAccountName() {
        return System.getProperty("user.name");
    }

    public static String getTempFolder() {
        return System.getProperty("java.io.tmpdir");
    }

    public static String pathToTagPath(@NotNull OS os, @NotNull String basicPath) {
        if (os == null || basicPath == null) {
            throw new IllegalArgumentException();
        }
        String path = basicPath;
        for (SpecificPath specificPath : os.getPaths()) {
            path = path.replace(specificPath.getPath(), specificPath.getTag());
        }
        return path;
    }

    public static String tagPathToPath(@NotNull OS os, @NotNull String pathWithTags) {
        String path = pathWithTags;
        for (SpecificPath specificPath : os.getPaths()) {
            path = path.replace(specificPath.getTag(), specificPath.getPath());
        }
        return path;
    }
}
