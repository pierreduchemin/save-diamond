package com.pierreduchemin.utils.jfx;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.stage.Window;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DialogBuilder {

    private Alert.AlertType alertType;
    private Window owner;
    private String title;
    private String headerText;
    private String content;
    private String iconUrl;
    private List<DialogButton> buttons;
    private Alert alert;

    public DialogBuilder(@NotNull Window owner, @NotNull Alert.AlertType alertType) {
        this.owner = owner;
        this.alertType = alertType;
    }

    public DialogBuilder setTitle(@NotNull String title) {
        this.title = title;
        return this;
    }

    public DialogBuilder setContent(@NotNull String content) {
        this.content = content;
        return this;
    }

    public DialogBuilder setHeaderText(@NotNull String headerText) {
        this.headerText = headerText;
        return this;
    }

    public DialogBuilder setIconUrl(@NotNull String iconUrl) {
        this.iconUrl = iconUrl;
        return this;
    }

    public DialogBuilder addButton(@NotNull DialogButton dialogButton) {
        if (buttons == null) {
            buttons = new ArrayList<>();
        }
        buttons.add(dialogButton);
        return this;
    }

    /**
     * Will be null if never displayed
     */
    public Alert getAlert() {
        return alert;
    }

    public void show() {
        alert = new Alert(alertType);
        alert.initOwner(owner);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(content);
        if (iconUrl != null) {
            alert.setGraphic(new ImageView(iconUrl));
        }

        if (buttons == null) {
            alert.show();
        } else {
            alert.getButtonTypes().clear();

            buttons.forEach(dialogButton ->
                    alert.getButtonTypes().add(dialogButton.getButtonType()));

            Optional<ButtonType> result = alert.showAndWait();
            result.ifPresent(
                    buttonType -> buttons.stream()
                            .filter(dialogButton -> dialogButton.getButtonListener() != null)
                            .filter(dialogButton -> buttonType == dialogButton.getButtonType())
                            .forEach(dialogButton -> dialogButton.getButtonListener().onClicked())
            );
        }
    }
}