package com.pierreduchemin.utils.jfx;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileChooserBuilder {

    private Window owner;
    private String title;
    private File initialDirectory;
    private List<FileChooser.ExtensionFilter> extensionFilters;
    private FileChooser fileChooser;
    private FileChooserMode fileChooserMode;

    public FileChooserBuilder(@NotNull Window owner, @NotNull FileChooserMode fileChooserMode) {
        this.owner = owner;
        this.fileChooserMode = fileChooserMode;
    }

    public FileChooserBuilder setTitle(@NotNull String title) {
        this.title = title;
        return this;
    }

    public FileChooserBuilder setInitialDirectory(@NotNull String initialDirectory) {
        return setInitialDirectory(new File(initialDirectory));
    }

    public FileChooserBuilder setInitialDirectory(@NotNull File initialDirectory) {
        this.initialDirectory = initialDirectory;
        return this;
    }

    public FileChooserBuilder addExtensionFilter(@NotNull FileChooser.ExtensionFilter extensionFilter) {
        if (this.extensionFilters == null) {
            this.extensionFilters = new ArrayList<>();
        }
        this.extensionFilters.add(extensionFilter);
        return this;
    }

    /**
     * Will be null if never displayed
     */
    public FileChooser getFileChooser() {
        return fileChooser;
    }

    public File show() {
        fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        if (initialDirectory != null) {
            fileChooser.setInitialDirectory(initialDirectory);
        }
        if (extensionFilters != null) {
            fileChooser.getExtensionFilters().addAll(extensionFilters);
        }

        switch (fileChooserMode) {
            case SAVE:
                return fileChooser.showSaveDialog(owner);
            case OPEN:
                return fileChooser.showOpenDialog(owner);
            default:
                throw new IllegalStateException();
        }
    }

    public enum FileChooserMode {
        SAVE, OPEN
    }
}
