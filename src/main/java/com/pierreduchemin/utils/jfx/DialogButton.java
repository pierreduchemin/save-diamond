package com.pierreduchemin.utils.jfx;

import javafx.scene.control.ButtonType;

public class DialogButton {

    private ButtonType buttonType;
    private ButtonListener buttonListener;

    public DialogButton(ButtonType buttonType, ButtonListener buttonListener) {
        this.buttonType = buttonType;
        this.buttonListener = buttonListener;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public ButtonListener getButtonListener() {
        return buttonListener;
    }

    public interface ButtonListener {
        void onClicked();
    }
}