package com.pierreduchemin.utils;

import java.util.ResourceBundle;

public class AppUtils {

    private AppUtils() {
    }

    public static String getAppVersion() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("build");
        return resourceBundle.getString("application.version");
    }
}
