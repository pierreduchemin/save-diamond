package com.pierreduchemin.data.configuration;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.SevenZipSavesArchiveFormat;
import com.pierreduchemin.data.archivers.ZipSavesArchiveFormat;
import com.pierreduchemin.data.configuration.model.OS;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Preferences {

    @Nullable
    private static Preferences instance;

    @NotNull
    private OS currentOs;
    @NotNull
    private SavesArchiveFormat defaultSavesArchiveFormat;
    private List<SavesArchiveFormat> availableSaveArchiveFormats;
    private boolean scanOnStart;
    private boolean askBeforeErasing;

    private Preferences() {
        this.currentOs = OsFactory.getCurrentOs();
        this.defaultSavesArchiveFormat = new SevenZipSavesArchiveFormat();
        this.availableSaveArchiveFormats = new ArrayList<>();
        this.availableSaveArchiveFormats.add(defaultSavesArchiveFormat);
        this.availableSaveArchiveFormats.add(new ZipSavesArchiveFormat());
        this.scanOnStart = true;
        this.askBeforeErasing = true;
    }

    @NotNull
    public static Preferences get() {
        if (instance == null) {
            instance = new Preferences();
        }
        return instance;
    }

    @NotNull
    public OS getOs() {
        return currentOs;
    }

    @NotNull
    public SavesArchiveFormat getDefaultSavesArchiveFormat() {
        return defaultSavesArchiveFormat;
    }

    public void setDefaultSavesArchiveFormat(@NotNull SavesArchiveFormat defaultSavesArchiveFormat) {
        this.defaultSavesArchiveFormat = defaultSavesArchiveFormat;
    }

    public List<SavesArchiveFormat> getAvailableSaveArchiveFormats() {
        return availableSaveArchiveFormats;
    }

    public boolean isScanOnStart() {
        return scanOnStart;
    }

    public void setScanOnStart(boolean scanOnStart) {
        this.scanOnStart = scanOnStart;
    }

    public boolean isAskBeforeErasing() {
        return askBeforeErasing;
    }
}
