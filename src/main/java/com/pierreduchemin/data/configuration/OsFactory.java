package com.pierreduchemin.data.configuration;

import com.pierreduchemin.data.archivers.exceptions.UnsupportedPlatformException;
import com.pierreduchemin.data.configuration.model.GnuLinux;
import com.pierreduchemin.data.configuration.model.OS;
import com.pierreduchemin.data.configuration.model.Windows;
import com.pierreduchemin.utils.OsUtils;
import org.jetbrains.annotations.NotNull;

class OsFactory {

    private OsFactory() {
    }

    @NotNull
    public static OS getCurrentOs() {
        String osName = OsUtils.getOsName();

        if (osName.contains("nix") || osName.contains("nux") || osName.contains("aix")) {
            return new GnuLinux(osName);
        }
        if (osName.contains("win")) {
            return new Windows(osName);
        }

        throw new UnsupportedPlatformException("Non supported platform");
    }
}
