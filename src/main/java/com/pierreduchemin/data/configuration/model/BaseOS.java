package com.pierreduchemin.data.configuration.model;

import com.pierreduchemin.utils.OsUtils;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public abstract class BaseOS implements OS {

    @NotNull
    private String jvmName;

    BaseOS(@NotNull String jvmName) {
        this.jvmName = jvmName;
    }

    @Override
    public @NotNull String getJvmName() {
        return jvmName;
    }

    @Override
    public @NotNull Path getAppTempPath() {
        return Paths.get(OsUtils.getTempFolder() + "/save-diamond-tmp/");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseOS baseOS = (BaseOS) o;
        return Objects.equals(jvmName, baseOS.jvmName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jvmName);
    }
}
