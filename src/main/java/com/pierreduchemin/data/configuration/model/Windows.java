package com.pierreduchemin.data.configuration.model;

import com.pierreduchemin.utils.OsUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class Windows extends BaseOS implements OS {

    public Windows(@NotNull String jvmName) {
        super(jvmName);
    }

    @Override
    public @NotNull List<SpecificPath> getPaths() {
        List<SpecificPath> specificPaths = new ArrayList<>();
//        specificPaths.add(new SpecificPath("[DOCUMENTS_AND_SETTINGS]", ""));
//        specificPaths.add(new SpecificPath("[MY_DOCUMENTS]", ""));
//        specificPaths.add(new SpecificPath("[PROGRAM_DATA]", ""));
//        specificPaths.add(new SpecificPath("[PROGRAM_FILES]", ""));
//        specificPaths.add(new SpecificPath("[STEAM]", ""));
        specificPaths.add(new SpecificPath("[HOME]", OsUtils.getUserHome()));
        specificPaths.add(new SpecificPath("[SEP]", OsUtils.getFileSeparator()));
        return specificPaths;
    }

    @Override
    public @NotNull String getReadableName() {
        return "Windows";
    }

    @Override
    public OsFamily getOsFamily() {
        return OsFamily.WINDOWS;
    }
}
