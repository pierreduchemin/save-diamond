package com.pierreduchemin.data.configuration.model;

import org.jetbrains.annotations.NotNull;

public class SpecificPath {

    private String tag;
    private String path;

    public SpecificPath(@NotNull String tag, @NotNull String path) {
        if (tag.isEmpty()) {
            throw new IllegalArgumentException("Invalid tag");
        }
        if (path.isEmpty()) {
            throw new IllegalArgumentException("Invalid path");
        }
        this.tag = tag;
        this.path = path;
    }

    public String getTag() {
        return tag;
    }

    public String getPath() {
        return path;
    }
}
