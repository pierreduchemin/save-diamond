package com.pierreduchemin.data.configuration.model;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.util.List;

public interface OS {

    @NotNull List<SpecificPath> getPaths();

    @NotNull
    String getReadableName();

    @NotNull Path getAppTempPath();

    @NotNull String getJvmName();

    OsFamily getOsFamily();

    enum OsFamily {
        UNIX, WINDOWS
    }
}
