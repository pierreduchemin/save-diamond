package com.pierreduchemin.data.configuration.model;

import com.pierreduchemin.utils.OsUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GnuLinux extends BaseOS implements OS {

    public GnuLinux(@NotNull String jvmName) {
        super(jvmName);
    }

    @NotNull
    @Override
    public List<SpecificPath> getPaths() {
        List<SpecificPath> specificPaths = new ArrayList<>();
        specificPaths.add(new SpecificPath("[HOME]", OsUtils.getUserHome() + OsUtils.getFileSeparator()));
        specificPaths.add(new SpecificPath("$HOME", OsUtils.getUserHome() + OsUtils.getFileSeparator()));
        specificPaths.add(new SpecificPath("~", OsUtils.getUserHome() + OsUtils.getFileSeparator()));
        specificPaths.add(new SpecificPath("[SEP]", OsUtils.getFileSeparator()));
        specificPaths.add(new SpecificPath("XDG_CONFIG_HOME", OsUtils.getUserHome() +
                OsUtils.getFileSeparator() + ".config" + OsUtils.getFileSeparator()));
        specificPaths.add(new SpecificPath("$XDG_DATA_HOME", OsUtils.getUserHome() +
                OsUtils.getFileSeparator() + ".local" + OsUtils.getFileSeparator() + "share" + OsUtils.getFileSeparator()));
        return specificPaths;
    }

    @NotNull
    @Override
    public String getReadableName() {
        return "GNU/Linux";
    }

    @Override
    public OsFamily getOsFamily() {
        return OsFamily.UNIX;
    }
}
