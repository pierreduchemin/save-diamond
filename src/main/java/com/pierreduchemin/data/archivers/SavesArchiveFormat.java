package com.pierreduchemin.data.archivers;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;

public interface SavesArchiveFormat {

    @NotNull String getArchiveFormatName();

    @NotNull String getArchiveExtension();

    void toArchive(@NotNull Path sourceFolder, @NotNull Path archiveFile) throws IOException;

    void fromArchive(@NotNull Path archiveFile, @NotNull Path destinationFolder) throws IOException;
}
