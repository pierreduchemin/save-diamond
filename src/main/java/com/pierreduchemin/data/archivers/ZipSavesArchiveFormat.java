package com.pierreduchemin.data.archivers;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipSavesArchiveFormat implements SavesArchiveFormat {

    @Override
    @NotNull
    public String getArchiveFormatName() {
        return "ZIP";
    }

    @Override
    @NotNull
    public String getArchiveExtension() {
        return ".zip";
    }

    @Override
    public void toArchive(@NotNull Path sourceFolder, @NotNull Path archiveFile) throws IOException {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(archiveFile.toFile()))) {
            Files.walkFileTree(sourceFolder, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                    zipOutputStream.putNextEntry(new ZipEntry(sourceFolder.relativize(file).toString()));
                    Files.copy(file, zipOutputStream);
                    zipOutputStream.closeEntry();

                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    @Override
    public void fromArchive(@NotNull Path archiveFile, @NotNull Path destinationFolder) throws IOException {
        byte[] buffer = new byte[1024];
        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(archiveFile.toFile()))) {
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            while (zipEntry != null) {
                File newFile = new File(destinationFolder.toAbsolutePath().toString() + File.separator + zipEntry.getName());
                FileUtils.forceMkdir(newFile.getParentFile());
                if (!newFile.createNewFile()) {
                    // the file already exists
                    return;
                }
                try (FileOutputStream fileOutputStream = new FileOutputStream(newFile)) {
                    int length;
                    while ((length = zipInputStream.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, length);
                    }
                    zipEntry = zipInputStream.getNextEntry();
                }
            }
            zipInputStream.closeEntry();
        }
    }
}
