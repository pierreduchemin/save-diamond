package com.pierreduchemin.data.archivers;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class SevenZipSavesArchiveFormat implements SavesArchiveFormat {

    @Override
    @NotNull
    public String getArchiveFormatName() {
        return "7-ZIP";
    }

    @Override
    @NotNull
    public String getArchiveExtension() {
        return ".7z";
    }

    @Override
    public void toArchive(@NotNull Path sourceFolder, @NotNull Path archiveFile) throws IOException {
        try (SevenZOutputFile sevenZOutputFile = new SevenZOutputFile(archiveFile.toFile())) {
            Files.walkFileTree(sourceFolder, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                    Path relativize = sourceFolder.relativize(file);
                    SevenZArchiveEntry entry = sevenZOutputFile.createArchiveEntry(relativize.toFile(), relativize.toString());
                    sevenZOutputFile.putArchiveEntry(entry);
                    sevenZOutputFile.write(Files.readAllBytes(file));
                    sevenZOutputFile.closeArchiveEntry();

                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    @Override
    public void fromArchive(@NotNull Path archiveFile, @NotNull Path destinationFolder) throws IOException {
        try (SevenZFile sevenZFile = new SevenZFile(archiveFile.toFile())) {
            SevenZArchiveEntry entry = sevenZFile.getNextEntry();
            while (entry != null) {
                File newFile = new File(destinationFolder.toAbsolutePath() + File.separator + entry.getName());
                FileUtils.forceMkdir(newFile.getParentFile());
                if (!newFile.createNewFile()) {
                    // the file already exists
                    return;
                }
                try (FileOutputStream fileOutputStream = new FileOutputStream(newFile)) {
                    byte[] content = new byte[(int) entry.getSize()];
                    sevenZFile.read(content, 0, content.length);
                    fileOutputStream.write(content);
                    entry = sevenZFile.getNextEntry();
                }
            }
        }
    }
}
