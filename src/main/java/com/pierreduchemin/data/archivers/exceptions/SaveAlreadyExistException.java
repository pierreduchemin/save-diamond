package com.pierreduchemin.data.archivers.exceptions;

public class SaveAlreadyExistException extends RuntimeException {

    private final String path;

    public SaveAlreadyExistException(String cause, String path) {
        super(cause);
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
