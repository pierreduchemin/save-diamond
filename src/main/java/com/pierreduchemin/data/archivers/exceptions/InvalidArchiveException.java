package com.pierreduchemin.data.archivers.exceptions;

public class InvalidArchiveException extends RuntimeException {

    public InvalidArchiveException(String s) {
        super(s);
    }
}
