package com.pierreduchemin.data.archivers.exceptions;

public class UnsupportedPlatformException extends RuntimeException {
    public UnsupportedPlatformException(String s) {
        super(s);
    }
}
