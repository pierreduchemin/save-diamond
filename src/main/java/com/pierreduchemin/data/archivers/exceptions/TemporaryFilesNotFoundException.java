package com.pierreduchemin.data.archivers.exceptions;

public class TemporaryFilesNotFoundException extends RuntimeException {

    public TemporaryFilesNotFoundException(String s) {
        super(s);
    }
}
