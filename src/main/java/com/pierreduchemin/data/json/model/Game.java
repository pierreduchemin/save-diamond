package com.pierreduchemin.data.json.model;

import com.pierreduchemin.data.configuration.model.OS;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Game {

    private int id;
    private String name;
    private Links links;
    private boolean isToSave;
    private Map<String, List<Path>> savePaths;

    public Game(int id, @NotNull String name, @Nullable Links links, boolean isToSave, @NotNull Map<String, List<Path>> savePaths) {
        if (id < 0 || name.trim().isEmpty() || name.contains("-")) {
            throw new IllegalArgumentException();
        }
        this.id = id;
        this.name = name;
        this.links = links;
        this.isToSave = isToSave;
        this.savePaths = savePaths;
    }

    public int getId() {
        return id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public Links getLinks() {
        return links;
    }

    public boolean isToSave() {
        return isToSave;
    }

    @NotNull
    public Map<String, List<Path>> getSavePaths() {
        return savePaths;
    }

    @NotNull
    public String getTempFolderName() {
        return String.format("%s-%s", id, name.replaceAll("\\s|![A-Za-z0-9\\-._]", "_"));
    }

    @NotNull
    public List<Path> getSavePaths(@NotNull OS os) {
        List<Path> paths = savePaths.get(os.getJvmName());
        if (paths == null) {
            return new ArrayList<>();
        }
        return paths;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Game game = (Game) o;
        return name.equals(game.name) && savePaths.equals(game.savePaths);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + savePaths.hashCode();
        return result;
    }

    public static class Links {

        private String pcgamingwiki;

        public Links(@Nullable String pcgamingwiki) {
            this.pcgamingwiki = pcgamingwiki;
        }

        @Nullable
        public String getPcgamingwiki() {
            return pcgamingwiki;
        }
    }
}
