package com.pierreduchemin.data.json.model;

import com.pierreduchemin.data.json.exceptions.InvalidGameSetException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Library {

    private static Library instance;

    private List<Game> knownGames;

    private Library() {
        this.knownGames = new ArrayList<>();
    }

    @NotNull
    public static Library getInstance() {
        if (instance == null) {
            instance = new Library();
        }
        return instance;
    }

    @NotNull
    public List<Game> getKnownGames() {
        return knownGames;
    }

    public void setGames(@Nullable List<Game> games) {
        if (games == null) {
            this.knownGames = new ArrayList<>();
            return;
        }
        if (containsDuplicateId(games)) {
            throw new InvalidGameSetException("Duplicate id in game set");
        }
        this.knownGames = games;
    }

    public void addGame(@NotNull Game game) {
        boolean alreadyInSet = knownGames.stream()
                .anyMatch(currentGame -> currentGame.getId() == game.getId());
        if (alreadyInSet) {
            throw new InvalidGameSetException("Duplicate id in game set");
        }
        this.knownGames.add(game);
    }

    public void addGames(@Nullable List<Game> games) {
        if (games == null) {
            return;
        }
        if (containsDuplicateId(games, knownGames)) {
            throw new InvalidGameSetException("Duplicate id in game set");
        }
        this.knownGames.addAll(games);
    }

    @SafeVarargs
    private final boolean containsDuplicateId(@Nullable List<Game>... games) {
        if (games == null) {
            return false;
        }

        List<Game> combined = new ArrayList<>();
        for (List<Game> gameList : games) {
            if (gameList != null) {
                combined.addAll(gameList);
            }
        }

        if (combined.isEmpty()) {
            return false;
        }

        for (int i = 0; i < combined.size(); i++) {
            Game g = combined.get(i);
            for (int j = 0; j < combined.size(); j++) {
                Game g1 = combined.get(j);
                if (g.getId() == g1.getId() && i != j) {
                    return true;
                }
            }
        }
        return false;
    }
}
