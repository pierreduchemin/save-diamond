package com.pierreduchemin.data.json.exceptions;

public class InvalidGameSetException extends RuntimeException {

    public InvalidGameSetException(String s) {
        super(s);
    }
}
