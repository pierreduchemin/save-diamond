package com.pierreduchemin.data.json.exceptions;

public class InvalidGameJsonException extends RuntimeException {

    public InvalidGameJsonException(String s) {
        super(s);
    }
}
