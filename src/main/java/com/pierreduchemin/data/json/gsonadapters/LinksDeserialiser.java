package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pierreduchemin.data.json.model.Game;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;

public class LinksDeserialiser implements JsonDeserializer<Game.Links> {

    private static LinksDeserialiser instance;

    private LinksDeserialiser() {
    }

    @NotNull
    public static LinksDeserialiser get() {
        if (instance == null) {
            instance = new LinksDeserialiser();
        }
        return instance;
    }

    @Override
    @NotNull
    public Game.Links deserialize(@NotNull JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String pcgamingwiki = jsonObject.get("pcgamingwiki").getAsString();
        return new Game.Links(pcgamingwiki);
    }
}
