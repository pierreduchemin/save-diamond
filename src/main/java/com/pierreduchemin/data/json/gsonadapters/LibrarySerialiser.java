package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.pierreduchemin.data.json.model.Library;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;

public class LibrarySerialiser implements JsonSerializer<Library> {

    private static LibrarySerialiser instance;

    private LibrarySerialiser() {
    }

    @NotNull
    public static LibrarySerialiser get() {
        if (instance == null) {
            instance = new LibrarySerialiser();
        }
        return instance;
    }

    @Override
    @NotNull
    public JsonElement serialize(@NotNull Library library, Type type, @NotNull JsonSerializationContext jsonSerializationContext) {
        JsonArray jsonArray = new JsonArray();
        library.getKnownGames().forEach(game -> jsonArray.add(jsonSerializationContext.serialize(game)));
        return jsonArray;
    }
}
