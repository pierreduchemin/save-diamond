package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.*;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

public class SavePathSerialiser implements JsonSerializer<Map<String, Set<Path>>> {

    private static SavePathSerialiser instance;

    private SavePathSerialiser() {
    }

    @NotNull
    public static SavePathSerialiser get() {
        if (instance == null) {
            instance = new SavePathSerialiser();
        }
        return instance;
    }

    @Override
    @NotNull
    public JsonElement serialize(@NotNull Map<String, Set<Path>> stringSetMap, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        stringSetMap.keySet().forEach(key -> {
            jsonObject.addProperty("os", key);
            JsonArray jsonArray = new JsonArray();
            stringSetMap.get(key).forEach(path -> jsonArray.add(path.toString()));
            jsonObject.add("paths", jsonArray);
        });
        return jsonObject;
    }
}
