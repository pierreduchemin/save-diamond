package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.pierreduchemin.data.json.model.Game;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

public class GameSerialiser implements JsonSerializer<Game> {

    private static GameSerialiser instance;

    private GameSerialiser() {
    }

    @NotNull
    public static GameSerialiser get() {
        if (instance == null) {
            instance = new GameSerialiser();
        }
        return instance;
    }

    @Override
    @NotNull
    public JsonElement serialize(@NotNull Game game, Type type, @NotNull JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", game.getId());
        jsonObject.addProperty("name", game.getName());
        jsonObject.add("savePaths", jsonSerializationContext.serialize(game.getSavePaths(), new TypeToken<Map<String, Set<Path>>>() {
        }.getType()));
        return jsonObject;
    }
}
