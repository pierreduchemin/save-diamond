package com.pierreduchemin.data.json.gsonadapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.json.exceptions.InvalidGameJsonException;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.utils.OsUtils;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameDeserializer implements JsonDeserializer<Game> {

    private static GameDeserializer instance;

    private GameDeserializer() {
    }

    @NotNull
    public static GameDeserializer get() {
        if (instance == null) {
            instance = new GameDeserializer();
        }
        return instance;
    }

    @Override
    @NotNull
    public Game deserialize(@NotNull JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if(!jsonObject.has("id") || !jsonObject.has("name") || !jsonObject.has("save_path")) {
            throw new InvalidGameJsonException("A game json must have an id, a name and a save_path entry");
        }
        int id = jsonObject.get("id").getAsInt();
        String name = jsonObject.get("name").getAsString();

        Game.Links links = null;
        if (jsonObject.has("links")) {
            links = jsonDeserializationContext.deserialize(jsonObject.get("links"), Game.Links.class);
        }

        Map<String, List<Path>> savePaths = new HashMap<>();
        jsonObject.get("save_path").getAsJsonArray().forEach(savePath -> {
            List<Path> paths = new ArrayList<>();
            savePath.getAsJsonObject().get("paths").getAsJsonArray().forEach(path -> paths.add(Paths.get(OsUtils.tagPathToPath(Preferences.get().getOs(), path.getAsString()))));
            savePaths.put(
                    savePath.getAsJsonObject().get("os").getAsString(),
                    paths
            );
        });
        return new Game(id, name, links,true, savePaths);
    }
}
