package com.pierreduchemin.data.json;

import com.google.gson.GsonBuilder;
import com.pierreduchemin.data.json.gsonadapters.GameDeserializer;
import com.pierreduchemin.data.json.gsonadapters.LinksDeserialiser;
import com.pierreduchemin.data.json.model.Game;
import org.jetbrains.annotations.NotNull;

import java.io.Reader;
import java.util.Arrays;
import java.util.List;

public class GameSerializer {

    private GameSerializer() {
    }

    public static List<Game> fromJson(@NotNull Reader reader) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Game.Links.class, LinksDeserialiser.get());
        gsonBuilder.registerTypeAdapter(Game.class, GameDeserializer.get());
        return Arrays.asList(gsonBuilder.create().fromJson(reader, Game[].class));
    }
}
