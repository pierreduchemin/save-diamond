package com.pierreduchemin.ui.actionmenu;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.ui.librarymanager.LibraryManagerViewImpl;
import com.pierreduchemin.utils.AppUtils;
import com.pierreduchemin.utils.OsUtils;
import com.pierreduchemin.utils.jfx.DialogBuilder;
import com.pierreduchemin.utils.jfx.FileChooserBuilder;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ActionMenuViewImpl implements ActionMenuView, Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionMenuViewImpl.class);
    @FXML
    public BorderPane bpActionMenu;

    private Stage stage;
    private ResourceBundle resourceBundle;

    @FXML
    private MenuItem miExit;
    @FXML
    private MenuItem miAbout;

    @FXML
    private Button btnSave;
    @FXML
    private Button btnRestore;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;

        miExit.setOnAction(event -> stage.close());

        miAbout.setOnAction(event -> showAboutDialog());
        btnSave.setOnAction(event -> launchSave());
        btnRestore.setOnAction(event -> showRestoreArchiveDialog());

        bpActionMenu.setOnDragOver(dragEvent -> {
            Dragboard dragboard = dragEvent.getDragboard();

            File archiveFile = dragboard.getFiles().get(0);
            // FIXME: must accept all known formats
            final boolean isAccepted = archiveFile.getName().toLowerCase().endsWith(Preferences.get().getDefaultSavesArchiveFormat().getArchiveExtension());
            if (dragboard.hasFiles() && isAccepted) {
                dragEvent.acceptTransferModes(TransferMode.COPY);
            } else {
                dragEvent.consume();
            }
        });

        bpActionMenu.setOnDragDropped(dragEvent -> {
            Dragboard dragboard = dragEvent.getDragboard();

            File archiveFile = dragboard.getFiles().get(0);
            if (dragboard.hasFiles()) {
                launchRestore(archiveFile);
            } else {
                dragEvent.consume();
            }
        });
    }

    @Override
    public void launchSave() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/library_manager.fxml"), resourceBundle);
        try {
            Stage libraryManagerStage = launchModalStage(loader, resourceBundle.getString("info.title.backup_game_saves"));

            LibraryManagerViewImpl libraryManagerView = loader.getController();
            libraryManagerView.setStage(libraryManagerStage);
            libraryManagerView.setMode(LibraryManagerViewImpl.Mode.SAVE);
        } catch (IOException e) {
            LOGGER.error("Not able to open fxml file", e);
        }
    }

    @Override
    public void showRestoreArchiveDialog() {
        SavesArchiveFormat savesArchiveFormat = Preferences.get().getDefaultSavesArchiveFormat();
        File restoreFile = new FileChooserBuilder(stage, FileChooserBuilder.FileChooserMode.OPEN)
                .setInitialDirectory(OsUtils.getUserHome())
                .addExtensionFilter(new FileChooser.ExtensionFilter(savesArchiveFormat.getArchiveFormatName(), "*" + savesArchiveFormat.getArchiveExtension()))
                .setTitle(resourceBundle.getString("info.title.archive_location"))
                .show();

        if (restoreFile != null) {
            launchRestore(restoreFile);
        }
    }

    private void launchRestore(@NotNull File restoreFile) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/library_manager.fxml"), resourceBundle);
        try {
            Stage libraryManagerStage = launchModalStage(loader, resourceBundle.getString("info.title.restore_game_saves"));

            LibraryManagerViewImpl libraryManagerView = loader.getController();
            libraryManagerView.setStage(libraryManagerStage);
            libraryManagerView.setRestoreFile(restoreFile);
            libraryManagerView.setMode(LibraryManagerViewImpl.Mode.RESTORE);
        } catch (IOException e) {
            LOGGER.error("Not able to open fxml file", e);
        }
    }

    private Stage launchModalStage(@NotNull FXMLLoader loader, @NotNull String title) throws IOException {
        Parent root = loader.load();

        Stage modalStage = new Stage();
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setTitle(title);
        modalStage.setScene(new Scene(root));
        modalStage.getIcons().add(new Image("/icons/app_icon/app_icon_32.png"));
        modalStage.show();

        return modalStage;
    }

    @Override
    public void showAboutDialog() {
        new DialogBuilder(stage, Alert.AlertType.INFORMATION)
                .setTitle(resourceBundle.getString("info.title.about"))
                .setHeaderText("Save Diamond")
                .setContent(String.format(resourceBundle.getString("info.message.about"), AppUtils.getAppVersion()))
                .setIconUrl("/icons/app_icon/app_icon_32.png")
                .show();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
