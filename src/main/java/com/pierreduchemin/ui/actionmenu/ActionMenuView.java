package com.pierreduchemin.ui.actionmenu;

import com.pierreduchemin.ui.base.MvpView;

import java.io.IOException;

public interface ActionMenuView extends MvpView {

    void launchSave() throws IOException;

    void showRestoreArchiveDialog() throws IOException;

    void showAboutDialog();
}
