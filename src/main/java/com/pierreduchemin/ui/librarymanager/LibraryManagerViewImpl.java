package com.pierreduchemin.ui.librarymanager;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.data.json.model.Library;
import com.pierreduchemin.ui.actionmenu.ActionMenuViewImpl;
import com.pierreduchemin.utils.BrowserUtils;
import com.pierreduchemin.utils.OsUtils;
import com.pierreduchemin.utils.jfx.DialogBuilder;
import com.pierreduchemin.utils.jfx.DialogButton;
import com.pierreduchemin.utils.jfx.FileChooserBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.control.MasterDetailPane;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.util.ResourceBundle;

public class LibraryManagerViewImpl implements LibraryManagerView, Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionMenuViewImpl.class);
    @FXML
    public Label lblGameName;
    @FXML
    public Hyperlink hlPcgamingwiki;
    @FXML
    public SeparatorMenuItem separator;
    private LibraryManagerPresenter<LibraryManagerView> libraryManagerPresenter;
    private Mode mode;
    private Stage stage;
    private ResourceBundle resourceBundle;
    @FXML
    private MenuItem miImport;
    @FXML
    private MenuItem miScan;
    @FXML
    private MenuItem miClose;
    @FXML
    private MasterDetailPane mdpMain;
    @FXML
    private TableView<Game> tvLibrary;
    private ObservableList<Game> olGames;
    @FXML
    private ListView<Path> lvPaths;
    @FXML
    private Button btnValid;
    @FXML
    private Button btnClose;

    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label lblStatus;
    private File restoreFile;

    public enum Mode {
        SAVE, RESTORE
    }

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        libraryManagerPresenter = new LibraryManagerPresenterImpl<>(resourceBundle);
        libraryManagerPresenter.onAttach(this);

        this.resourceBundle = resourceBundle;

        miImport.setOnAction(event -> showImportFileDialog());
        miScan.setOnAction(event -> scanSystem());
        miClose.setOnAction(event -> stage.close());

        bindToList();

        btnValid.setOnAction(event -> selectAction(mode));
        btnClose.setOnAction(event -> closeDetails());
    }

    private void selectAction(Mode mode) {
        if (mode == Mode.SAVE) {
            showSaveArchiveDialog();
        } else if (mode == Mode.RESTORE) {
            showRestoreArchiveDialog();
        }
    }

    private void bindToList() {
        TableColumn<Game, String> tcName = new TableColumn<>(resourceBundle.getString("info.title.game"));
        tcName.setMinWidth(100);
        tcName.setCellValueFactory((TableColumn.CellDataFeatures<Game, String> feature) -> new SimpleStringProperty(feature.getValue().getName()));
        tvLibrary.getColumns().add(tcName);
        tvLibrary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                setDetails(newSelection);
            }
        });
        olGames = FXCollections.observableArrayList(Library.getInstance().getKnownGames());
        tvLibrary.setItems(olGames);
    }

    private void setDetails(@NotNull Game game) {
        mdpMain.setShowDetailNode(true);
        lblGameName.setText(game.getName());
        Game.Links links = game.getLinks();
        if (links != null && links.getPcgamingwiki() != null) {
            hlPcgamingwiki.setText(links.getPcgamingwiki());
            hlPcgamingwiki.setOnAction(actionEvent -> BrowserUtils.openUrl(hlPcgamingwiki.getText()));
        } else {
            hlPcgamingwiki.setText("");
        }
        lvPaths.setItems(FXCollections.observableArrayList(game.getSavePaths(Preferences.get().getOs())));
    }

    private void closeDetails() {
        mdpMain.setShowDetailNode(false);
        tvLibrary.getSelectionModel().clearSelection();
    }

    @Override
    public void refreshGameList() {
        olGames.clear();
        olGames.addAll(Library.getInstance().getKnownGames());
        updateStatus("");
    }

    @Override
    public void showImportFileDialog() {
        File file = new FileChooserBuilder(stage, FileChooserBuilder.FileChooserMode.OPEN)
                .setInitialDirectory(OsUtils.getUserHome())
                .setTitle(resourceBundle.getString("info.title.choose_a_file_to_import"))
                .addExtensionFilter(new FileChooser.ExtensionFilter("JSON", "*.json"))
                .show();

        if (file != null) {
            libraryManagerPresenter.importJsonFile(file);
        }
    }

    @Override
    public void scanSystem() {
        libraryManagerPresenter.scanSystem();
        closeDetails();
    }

    @Override
    public void showSaveArchiveDialog() {
        if (Library.getInstance().getKnownGames().isEmpty()) {
            new DialogBuilder(stage, Alert.AlertType.WARNING)
                    .setTitle(resourceBundle.getString("info.title.nothing_to_save"))
                    .setHeaderText(resourceBundle.getString("info.message.no_save_found"))
                    .setContent(resourceBundle.getString("info.message.you_have_to_list_games_before_you_can_create_an_archive"))
                    .show();

            updateStatus(resourceBundle.getString("info.message.import_file_tip"));
            return;
        }

        File file = new FileChooserBuilder(stage, FileChooserBuilder.FileChooserMode.SAVE)
                .setInitialDirectory(OsUtils.getUserHome())
                .setTitle(resourceBundle.getString("info.title.archive_location"))
                .addExtensionFilter(new FileChooser.ExtensionFilter(Preferences.get().getDefaultSavesArchiveFormat().getArchiveFormatName(), "*" + Preferences.get().getDefaultSavesArchiveFormat().getArchiveExtension()))
                .show();

        if (file != null) {
            SavesArchiveFormat savesArchiveFormat = Preferences.get().getDefaultSavesArchiveFormat();
            if (!file.toString().endsWith(savesArchiveFormat.getArchiveExtension())) {
                file = new File(file.getAbsolutePath() + savesArchiveFormat.getArchiveExtension());
                if (file.exists()) {
                    final File finalFile = file;

                    new DialogBuilder(stage, Alert.AlertType.CONFIRMATION)
                            .setTitle(resourceBundle.getString("info.title.replace_existing_file"))
                            .setContent(resourceBundle.getString("info.message.replace_existing_file"))
                            .addButton(new DialogButton(ButtonType.YES, () -> libraryManagerPresenter.saveArchive(finalFile)))
                            .addButton(new DialogButton(ButtonType.NO, null))
                            .show();
                } else {
                    libraryManagerPresenter.saveArchive(file);
                }
                return;
            }
            libraryManagerPresenter.saveArchive(file);
        }
    }

    @Override
    public void showRestoreArchiveDialog() {
        if (restoreFile != null) {
            new DialogBuilder(stage, Alert.AlertType.CONFIRMATION)
                    .setTitle(resourceBundle.getString("info.title.replace_existing_saves"))
                    .setContent(resourceBundle.getString("info.message.replace_existing_saves"))
                    .addButton(new DialogButton(ButtonType.YES, () -> libraryManagerPresenter.restoreArchive(restoreFile, true)))
                    .addButton(new DialogButton(ButtonType.NO, () -> libraryManagerPresenter.restoreArchive(restoreFile, false)))
                    .show();
        }
    }

    @Override
    public void updateStatus(@Nullable String message) {
        String currentMessage;
        if (message == null) {
            currentMessage = "";
        } else {
            currentMessage = message;
        }
        lblStatus.setText(currentMessage);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisible(true);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisible(false);
    }

    @Override
    public void showMessage(Alert.AlertType alertType, String message) {
        new DialogBuilder(stage, alertType)
                .setTitle(resourceBundle.getString("program_name"))
                .setContent(message)
                .addButton(new DialogButton(ButtonType.OK, null))
                .show();
    }

    public void setStage(@NotNull Stage stage) {
        this.stage = stage;
    }

    public void setMode(@NotNull Mode mode) {
        this.mode = mode;
        onStart();
    }

    public void setRestoreFile(@NotNull File restoreFile) {
        this.restoreFile = restoreFile;
    }

    private void onStart() {
        if (mode == Mode.SAVE) {
            btnValid.setText(resourceBundle.getString("info.button.save"));
            btnValid.setTooltip(new Tooltip(resourceBundle.getString("info.tooltip.save")));

            libraryManagerPresenter.importJsonFile(new InputStreamReader(getClass().getResourceAsStream("/default_list.json")));
            if (Preferences.get().isScanOnStart()) {
                libraryManagerPresenter.scanSystem();
            }
        } else if (mode == Mode.RESTORE) {
            miImport.setVisible(false);
            miScan.setVisible(false);
            separator.setVisible(false);

            btnValid.setText(resourceBundle.getString("info.button.restore"));
            btnValid.setTooltip(new Tooltip(resourceBundle.getString("info.tooltip.restore")));

            libraryManagerPresenter.scanArchive(Preferences.get().getDefaultSavesArchiveFormat(), restoreFile);
        }
    }
}
