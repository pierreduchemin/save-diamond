package com.pierreduchemin.ui.librarymanager;

import com.google.gson.JsonSyntaxException;
import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.data.archivers.exceptions.InvalidArchiveException;
import com.pierreduchemin.data.configuration.Preferences;
import com.pierreduchemin.data.json.GameSerializer;
import com.pierreduchemin.data.json.exceptions.InvalidGameSetException;
import com.pierreduchemin.data.json.model.Game;
import com.pierreduchemin.data.json.model.Library;
import com.pierreduchemin.domain.SaveOrganiserImpl;
import com.pierreduchemin.ui.base.BasePresenter;
import javafx.scene.control.Alert;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.ResourceBundle;

public class LibraryManagerPresenterImpl<V extends LibraryManagerView> extends BasePresenter<V> implements LibraryManagerPresenter<V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryManagerPresenterImpl.class);

    @NotNull
    private final ResourceBundle resourceBundle;

    LibraryManagerPresenterImpl(@NotNull ResourceBundle resourceBundle) {
        super();
        this.resourceBundle = resourceBundle;
    }

    @Override
    public void scanSystem() {
        getView().showProgressBar();

        Library.getInstance().setGames(SaveOrganiserImpl.scanSystem());

        getView().refreshGameList();

        int nbGamesFound = Library.getInstance().getKnownGames().size();
        getView().updateStatus(getNbGamesFoundMessage(nbGamesFound));

        getView().hideProgressBar();
    }

    @Override
    public void scanArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull File archiveFile) {
        getView().showProgressBar();

        try {
            List<Game> games = SaveOrganiserImpl.scanArchive(savesArchiveFormat, archiveFile);
            Library.getInstance().setGames(games);

            getView().refreshGameList();

            int nbGamesFound = Library.getInstance().getKnownGames().size();
            getView().updateStatus(getNbGamesFoundMessage(nbGamesFound));
        } catch (InvalidArchiveException | InvalidGameSetException e) {
            getView().updateStatus(resourceBundle.getString("error.message.bad_archive_failed_to_restore_saves"));
            LOGGER.error("InvalidArchiveException | InvalidGameSetException occurred. Not able to read archive file.", e);
        } catch (IOException e) {
            getView().updateStatus(resourceBundle.getString("error.message.not_able_to_restore_game_saves"));
            LOGGER.error("IOException occurred. Not able to read archive file.", e);
        } finally {
            getView().hideProgressBar();
        }
    }

    private String getNbGamesFoundMessage(int nbGamesFound) {
        String message;
        if (nbGamesFound <= 0) {
            message = resourceBundle.getString("info.message.no_saves_found");
        } else if (nbGamesFound == 1) {
            message = String.format(resourceBundle.getString("info.message.one_save_found"), nbGamesFound);
        } else {
            message = String.format(resourceBundle.getString("info.message.saves_found"), nbGamesFound);
        }
        return message;
    }

    @Override
    public void importJsonFile(@NotNull File file) {
        getView().showProgressBar();

        try {
            Library.getInstance().setGames(GameSerializer.fromJson(new FileReader(file)));
            getView().refreshGameList();
        } catch (JsonSyntaxException e) {
            getView().updateStatus(resourceBundle.getString("error.message.invalid_json_file"));
            LOGGER.error("JsonSyntaxException occurred", e);
        } catch (FileNotFoundException e) {
            getView().updateStatus(resourceBundle.getString("error.message.file_not_found"));
            LOGGER.error("Not able to find file", e);
        } finally {
            getView().hideProgressBar();
        }
    }

    @Override
    public void importJsonFile(Reader reader) {
        getView().showProgressBar();

        try {
            Library.getInstance().setGames(GameSerializer.fromJson(reader));
            getView().refreshGameList();
        } catch (JsonSyntaxException e) {
            getView().updateStatus(resourceBundle.getString("error.message.invalid_json_file"));
            LOGGER.error("JsonSyntaxException occurred", e);
        } finally {
            getView().hideProgressBar();
        }
    }

    @Override
    public void saveArchive(@NotNull File file) {
        getView().showProgressBar();

        SavesArchiveFormat savesArchiveFormat = Preferences.get().getDefaultSavesArchiveFormat();
        try {
            SaveOrganiserImpl.toArchive(savesArchiveFormat, file.toPath(), Library.getInstance());
            getView().hideProgressBar();
            getView().showMessage(
                    Alert.AlertType.CONFIRMATION,
                    String.format(resourceBundle.getString("info.message.game_save_stored"), file.getAbsolutePath()));
        } catch (IOException e) {
            getView().showMessage(Alert.AlertType.ERROR,
                    String.format(resourceBundle.getString("error.message.not_able_to_write_file"), file.getAbsoluteFile()));
            LOGGER.error("IOException occurred. Not able to write archive file.", e);
        } finally {
            getView().hideProgressBar();
        }
    }

    @Override
    public void restoreArchive(@NotNull File file, boolean eraseExistingFiles) {
        getView().showProgressBar();

        try {
            SaveOrganiserImpl.fromArchive(Preferences.get().getDefaultSavesArchiveFormat(), file, eraseExistingFiles);
            getView().updateStatus(resourceBundle.getString("info.message.game_saves_restored"));
        } catch (InvalidArchiveException e) {
            getView().updateStatus(resourceBundle.getString("error.message.bad_archive_failed_to_restore_saves"));
            LOGGER.error("InvalidArchiveException occurred. Not able to read archive file.", e);
        } catch (IOException e) {
            getView().updateStatus(resourceBundle.getString("error.message.not_able_to_restore_game_saves"));
            LOGGER.error("IOException occurred. Not able to read archive file.", e);
        } finally {
            getView().hideProgressBar();
        }
    }
}
