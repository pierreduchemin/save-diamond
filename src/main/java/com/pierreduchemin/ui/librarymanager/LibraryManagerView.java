package com.pierreduchemin.ui.librarymanager;

import com.pierreduchemin.ui.base.MvpView;
import javafx.scene.control.Alert;
import org.jetbrains.annotations.Nullable;

public interface LibraryManagerView extends MvpView {

    void refreshGameList();

    void showImportFileDialog();

    void scanSystem();

    void showSaveArchiveDialog();

    void showRestoreArchiveDialog();

    void updateStatus(@Nullable String message);

    void showProgressBar();

    void hideProgressBar();

    void showMessage(Alert.AlertType alertType, String message);
}
