package com.pierreduchemin.ui.librarymanager;

import com.pierreduchemin.data.archivers.SavesArchiveFormat;
import com.pierreduchemin.ui.base.MvpPresenter;
import com.pierreduchemin.ui.base.MvpView;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.Reader;

public interface LibraryManagerPresenter<V extends MvpView> extends MvpPresenter<V> {

    void scanArchive(@NotNull SavesArchiveFormat savesArchiveFormat, @NotNull File archiveFile);

    void importJsonFile(@NotNull File file);

    void importJsonFile(Reader path);

    void saveArchive(@NotNull File file);

    void restoreArchive(@NotNull File file, boolean eraseExistingFile);

    void scanSystem();
}
