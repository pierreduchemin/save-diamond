package com.pierreduchemin.ui.base;

import org.jetbrains.annotations.NotNull;

public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V mvpView;

    @Override
    public void onAttach(@NotNull V mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mvpView = null;
    }

    @Override
    public V getView() {
        return mvpView;
    }
}
