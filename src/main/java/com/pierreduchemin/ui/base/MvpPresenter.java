package com.pierreduchemin.ui.base;

import org.jetbrains.annotations.NotNull;

public interface MvpPresenter<V extends MvpView> {

    void onAttach(@NotNull V mvpView);

    void onDetach();

    V getView();
}
