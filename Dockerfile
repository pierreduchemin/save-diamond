FROM ubuntu:18.04

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
    openjdk-8-jdk \
    openjfx \
    wget \
    unzip \
    p7zip-full

RUN wget https://services.gradle.org/distributions/gradle-4.8-bin.zip

RUN mkdir /opt/gradle

RUN unzip gradle-4.8-bin.zip -d /opt/gradle

RUN rm gradle-4.8-bin.zip

ENV PATH="/opt/gradle/gradle-4.8/bin:${PATH}"
