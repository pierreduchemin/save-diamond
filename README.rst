============
Save Diamond
============

Brief
=====

Sometimes, dealing with game saves is hard.
If you want to keep your data, you have to:

#. List all the games on your system (long)
#. Find where they write their saves (long)
#. Sort this a bit and backup the saves in a folder
#. Copy all saves one by one to their destination (long)

...and you know this job is unpleasant!

Save Diamond's goal is to help you do this.
It also have a few extra features such as cross-platform capabilities.

*With this utility, you can start a game on Windows and finish it on your favorite GNU/Linux distro!*

Now, all you have to do is:

#. Click 'Save button'
#. Keep the produced archive in a safe place
#. Click the 'Restore' button and provide the archive

Features:

#. Easy and fast
#. Backup all your saves in a single archive
#. Cross-platform
#. Tested
